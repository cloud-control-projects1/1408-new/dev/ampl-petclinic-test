import { useMemo, useCallback, useEffect, useState } from "react";
import { useLazyQuery } from "@apollo/client";
import { ResultOf } from "@graphql-typed-document-node/core";
import {
  Button,
  Card,
  Form,
  FormInstance,
  Input,
  InputNumber,
  Checkbox,
  message,
  Space,
  Spin
} from "antd";
import { useForm } from "antd/es/form/Form";
import { DatePicker } from "@amplicode/react";
import { gql } from "@amplicode/gql";
import { useNavigate, useParams } from "react-router-dom";
import { RequestFailedError } from "../../../core/crud/RequestFailedError";
import { useSubmitEditor } from "../../../core/crud/useSubmitEditor";
import { ErrorMessages } from "../../../core/crud/ErrorMessages";
import { FormattedMessage, useIntl } from "react-intl";
import { RefetchQueries } from "../../../core/type-aliases/RefetchQueries";
import { deserialize } from "../../../core/transform/model/deserialize";
import { useBreadcrumbItem } from "../../../core/screen/useBreadcrumbItem";
import { assertTrueValidator } from "../../../core/crud/validators/assertTrueValidator";
import { futureInstantValidator } from "../../../core/crud/validators/temporal/futureInstantValidator";
import { futureOrPresentDateValidator } from "../../../core/crud/validators/temporal/futureOrPresentDateValidator";
import { negativeValidator } from "../../../core/crud/validators/numeric/negativeValidator";
import { negativeOrZeroValidator } from "../../../core/crud/validators/numeric/negativeOrZeroValidator";
import { nullValidator } from "../../../core/crud/validators/nullValidator";
import { pastInstantValidator } from "../../../core/crud/validators/temporal/pastInstantValidator";
import { pastOrPresentInstantValidator } from "../../../core/crud/validators/temporal/pastOrPresentInstantValidator";
import { positiveValidator } from "../../../core/crud/validators/numeric/positiveValidator";
import { positiveOrZeroValidator } from "../../../core/crud/validators/numeric/positiveOrZeroValidator";
import { compareValidator } from "../../../core/crud/validators/numeric/compareValidator";
import { digitsValidator } from "../../../core/crud/validators/numeric/digitsValidator";
import { assertFalseValidator } from "../../../core/crud/validators/assertFalseValidator";

const CLIENT_VALIDATION_TEST_ENTITY = gql(`
  query Get_Client_Validation_Test_Entity($id: ID!) {
    clientValidationTestEntity(id: $id) {
      id
      businessEmail 
      email 
      eulaAccepted 
      future 
      futureOrPresent 
      id 
      length 
      negative 
      negativeOrZero 
      notBlank 
      notEmpty 
      nullField 
      past 
      pastOrPresent 
      pattern 
      positive 
      positiveOrZero 
      price 
      quantity 
      requiredWithoutDirective 
      size 
      trialExpired 
      urlString 
      urlWithoutDirective 
    }
  }
`);

const UPDATE_CLIENT_VALIDATION_TEST_ENTITY = gql(`
  mutation Update_Client_Validation_Test_Entity($input: ClientValidationTestEntityInput!) {
    updateClientValidationTestEntity(input: $input) {
      id
    }
  }
`);

export interface ClientValidationTestEntityCardsEditorProps<TData = any> {
  /**
   * A list of queries that needs to be refetched once the editor has been submitted.
   * For example, you might need to refresh entity list after editing an entity instance.
   * In simple cases this would be just an array of query names, e.g. ["Get_Pet_List"],
   * or an array of `DocumentNode`s, e.g. [PET_LIST].
   * For more info, check Apollo Client documentation.
   */
  refetchQueries?: RefetchQueries<TData>;
}

export function ClientValidationTestEntityCardsEditor({
  refetchQueries
}: ClientValidationTestEntityCardsEditorProps<QueryResultType>) {
  const intl = useIntl();
  useBreadcrumbItem(
    intl.formatMessage({ id: "screen.ClientValidationTestEntityCardsEditor" })
  );

  const { recordId } = useParams();

  if (recordId == null) throw new Error("recordId must be defined");
  // Load the item if `id` is provided
  const { item, itemLoading, itemError } = useLoadItem(recordId);

  if (itemLoading) {
    return <Spin />;
  }

  if (itemError) {
    return <RequestFailedError />;
  }

  return (
    <EditorForm item={item} id={recordId} refetchQueries={refetchQueries} />
  );
}

interface EditorFormProps<TData> {
  /**
   * Loaded entity instance (if editing).
   */
  item?: ItemType;
  /**
   *
   */
  id?: string;
  /**
   *
   */
  refetchQueries?: RefetchQueries<TData>;
}

function EditorForm<TData>({
  item,
  refetchQueries,
  id
}: EditorFormProps<TData>) {
  const [form] = useForm();

  // Global error message, i.e. error message not related to a particular form field.
  // Examples: cross-validation, network errors.
  const [formErrors, setFormErrors] = useState<string[]>([]);

  const { handleSubmit, submitting } = useSubmitEditor(
    UPDATE_CLIENT_VALIDATION_TEST_ENTITY,
    setFormErrors,
    form.setFields,
    refetchQueries,
    "ClientValidationTestEntityInput",
    id
  );
  const handleClientValidationFailed = useClientValidationFailed();

  // Put the item into the form.
  // Item becomes form field values, which will then be used inside `handleSubmit`.
  useFormData(form, item);

  return (
    <Card className="narrow-layout">
      <Form
        onFinish={handleSubmit}
        onFinishFailed={handleClientValidationFailed}
        layout="vertical"
        form={form}
      >
        <FormFields />
        <ErrorMessages errorMessages={formErrors} />
        <FormButtons submitting={submitting} />
      </Form>
    </Card>
  );
}

function FormFields() {
  const intl = useIntl();

  return (
    <>
      <Form.Item
        name="businessEmail"
        label="Business Email"
        rules={[
          {
            type: "email"
          },
          { pattern: /.*@haulmont.com$/ }
        ]}
      >
        <Input autoFocus />
      </Form.Item>

      <Form.Item
        name="email"
        label="Email"
        rules={[
          {
            type: "email"
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="eulaAccepted"
        label="Eula Accepted"
        rules={[
          {
            validator: assertTrueValidator,
            message: intl.formatMessage({ id: "form.validation.assertTrue" })
          },
          {
            required: true
          }
        ]}
        valuePropName="checked"
        initialValue={false}
      >
        <Checkbox />
      </Form.Item>

      <Form.Item
        name="future"
        label="Future"
        rules={[
          {
            validator: futureInstantValidator,
            message: intl.formatMessage({ id: "form.validation.futureInstant" })
          }
        ]}
      >
        <DatePicker showTime={{ format: "HH:mm:ss" }} />
      </Form.Item>

      <Form.Item
        name="futureOrPresent"
        label="Future Or Present"
        rules={[
          {
            validator: futureOrPresentDateValidator,
            message: intl.formatMessage({
              id: "form.validation.futureOrPresentDate"
            })
          }
        ]}
      >
        <DatePicker />
      </Form.Item>

      <Form.Item
        name="length"
        label="Length"
        rules={[
          {
            type: "string",
            min: 3,
            max: 5
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="negative"
        label="Negative"
        rules={[
          {
            validator: negativeValidator,
            message: intl.formatMessage({ id: "form.validation.negative" })
          },
          {
            required: true
          }
        ]}
      >
        <InputNumber type="number" />
      </Form.Item>

      <Form.Item
        name="negativeOrZero"
        label="Negative Or Zero"
        rules={[
          {
            validator: negativeOrZeroValidator,
            message: intl.formatMessage({
              id: "form.validation.negativeOrZero"
            })
          },
          {
            required: true
          }
        ]}
      >
        <InputNumber
          type="number"
          precision={0}
          max={2147483647}
          min={-2147483648}
        />
      </Form.Item>

      <Form.Item
        name="notBlank"
        label="Not Blank"
        rules={[
          {
            required: true,
            whitespace: true
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="notEmpty"
        label="Not Empty"
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="nullField"
        label="Null Field"
        rules={[
          {
            validator: nullValidator,
            message: intl.formatMessage({ id: "form.validation.null" })
          }
        ]}
      >
        <InputNumber
          type="number"
          precision={0}
          max={2147483647}
          min={-2147483648}
        />
      </Form.Item>

      <Form.Item
        name="past"
        label="Past"
        rules={[
          {
            validator: pastInstantValidator,
            message: intl.formatMessage({ id: "form.validation.pastInstant" })
          }
        ]}
      >
        <DatePicker showTime={{ format: "HH:mm:ss" }} />
      </Form.Item>

      <Form.Item
        name="pastOrPresent"
        label="Past Or Present"
        rules={[
          {
            validator: pastOrPresentInstantValidator,
            message: intl.formatMessage({
              id: "form.validation.pastOrPresentInstant"
            })
          }
        ]}
      >
        <DatePicker showTime={{ format: "HH:mm:ss" }} />
      </Form.Item>

      <Form.Item
        name="pattern"
        label="Pattern"
        rules={[
          {
            pattern: /^A.*/
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="positive"
        label="Positive"
        rules={[
          {
            validator: positiveValidator,
            message: intl.formatMessage({ id: "form.validation.positive" })
          }
        ]}
      >
        <InputNumber
          type="number"
          precision={0}
          max={2147483647}
          min={-2147483648}
        />
      </Form.Item>

      <Form.Item
        name="positiveOrZero"
        label="Positive Or Zero"
        rules={[
          {
            validator: positiveOrZeroValidator,
            message: intl.formatMessage({
              id: "form.validation.positiveOrZero"
            })
          }
        ]}
      >
        <InputNumber type="number" precision={0} stringMode />
      </Form.Item>

      <Form.Item
        name="price"
        label="Price"
        rules={[
          {
            validator: compareValidator.bind(null, {
              max: "199.99",
              includeMax: false
            }),
            message: intl.formatMessage(
              { id: "form.validation.lessThanMax" },
              { max: "199.99" }
            )
          },
          {
            validator: compareValidator.bind(null, {
              min: "150",
              includeMin: false
            }),
            message: intl.formatMessage(
              { id: "form.validation.greaterThanMin" },
              { min: "150" }
            )
          },
          {
            validator: digitsValidator.bind(null, {
              integer: 3,
              fraction: 2
            }),
            message: intl.formatMessage(
              { id: "form.validation.digits" },
              { integer: 3, fraction: 2 }
            )
          }
        ]}
      >
        <InputNumber type="number" stringMode />
      </Form.Item>

      <Form.Item
        name="quantity"
        label="Quantity"
        rules={[
          {
            validator: compareValidator.bind(null, {
              max: "10000",
              includeMax: false
            }),
            message: intl.formatMessage(
              { id: "form.validation.lessThanMax" },
              { max: "10000" }
            )
          },
          {
            validator: compareValidator.bind(null, {
              min: "500",
              includeMin: false
            }),
            message: intl.formatMessage(
              { id: "form.validation.greaterThanMin" },
              { min: "500" }
            )
          }
        ]}
      >
        <InputNumber type="number" precision={0} stringMode />
      </Form.Item>

      <Form.Item
        name="requiredWithoutDirective"
        label="Required Without Directive"
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="size"
        label="Size"
        rules={[
          {
            type: "string",
            min: 3,
            max: 5
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="trialExpired"
        label="Trial Expired"
        rules={[
          {
            validator: assertFalseValidator,
            message: intl.formatMessage({ id: "form.validation.assertFalse" })
          }
        ]}
        valuePropName="checked"
        initialValue={false}
      >
        <Checkbox />
      </Form.Item>

      <Form.Item
        name="urlString"
        label="Url String"
        rules={[
          {
            type: "url"
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="urlWithoutDirective"
        label="Url Without Directive"
        rules={[
          {
            type: "url"
          }
        ]}
      >
        <Input type="url" />
      </Form.Item>
    </>
  );
}

/**
 * Buttons below the form.
 *
 * @param submitting flag indicating whether submit is in progress
 */
function FormButtons({ submitting }: { submitting?: boolean }) {
  const navigate = useNavigate();

  return (
    <Form.Item className="form-buttons">
      <Space>
        <Button htmlType="button" onClick={() => navigate("..")}>
          <FormattedMessage id="common.cancel" />
        </Button>
        <Button type="primary" htmlType="submit" loading={submitting}>
          <FormattedMessage id={"common.submit"} />
        </Button>
      </Space>
    </Form.Item>
  );
}

/**
 * Loads the item if `id` is provided
 *
 * @param id
 */
function useLoadItem(id: string) {
  // Get the function that will load item from server,
  // also get variables that will contain loading/error state and response data
  // once the response is received
  const [loadItem, { loading, error, data }] = useLazyQuery(
    CLIENT_VALIDATION_TEST_ENTITY,
    {
      variables: {
        id
      }
    }
  );

  // Load item if `id` has been provided in props
  useEffect(() => {
    if (id !== "new") {
      loadItem();
    }
  }, [loadItem, id]);

  const item = useMemo(() => deserialize(data?.clientValidationTestEntity), [
    data?.clientValidationTestEntity
  ]);

  return {
    item,
    itemLoading: loading,
    itemError: error
  };
}

/**
 * Puts the `item` inside the `form`
 *
 * @param form
 * @param item
 */
function useFormData(form: FormInstance, item?: ItemType) {
  useEffect(() => {
    if (item != null) {
      form.setFieldsValue(item);
    }
  }, [item, form]);
}

/**
 * Returns a callback that is executed when client-side validation of a form has failed
 */
function useClientValidationFailed() {
  const intl = useIntl();

  return useCallback(() => {
    return message.error(
      intl.formatMessage({ id: "EntityDetailsScreen.validationError" })
    );
  }, [intl]);
}

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof CLIENT_VALIDATION_TEST_ENTITY>;
/**
 * Type of the item loaded by executing the query
 */
type ItemType = QueryResultType["clientValidationTestEntity"];
