import { Card, Spin, Empty, Descriptions, Button } from "antd";
import { gql } from "@amplicode/gql";
import { useQuery } from "@apollo/client";
import { FormattedMessage, useIntl } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { getPetDtoDisplayName } from "../../../core/display-name/getPetDtoDisplayName";
import { getPetTypeDtoDisplayName } from "../../../core/display-name/getPetTypeDtoDisplayName";
import { getOwnerDtoDisplayName } from "../../../core/display-name/getOwnerDtoDisplayName";
import { getPetDescriptionDtoDisplayName } from "../../../core/display-name/getPetDescriptionDtoDisplayName";
import { getTagDtoDisplayName } from "../../../core/display-name/getTagDtoDisplayName";
import { getPetDiseaseDtoDisplayName } from "../../../core/display-name/getPetDiseaseDtoDisplayName";
import { RequestFailedError } from "../../../core/crud/RequestFailedError";
import { deserialize } from "../../../core/transform/model/deserialize";
import { useBreadcrumbItem } from "../../../core/screen/useBreadcrumbItem";

const PET = gql(`
  query Get_Pet($id: ID!) {
    pet(id: $id) {
      id
      identificationNumber
      birthDate
      type {
        id
        name
      }
      owner {
        id
        firstName
        lastName
      }
      description {
        id
        description
      }
      tags {
        id
        name
      }
      diseases {
        id
        name
        description
      }      
    }
  }
`);

export function StandalonePetDetails() {
  const intl = useIntl();
  useBreadcrumbItem(intl.formatMessage({ id: "screen.StandalonePetDetails" }));

  const { recordId } = useParams();
  const navigate = useNavigate();

  if (recordId == null) throw new Error("recordId must be defined");
  const { loading: queryLoading, error: queryError, data } = useQuery(PET, {
    variables: {
      id: recordId
    }
  });

  const item = deserialize(data?.pet);

  if (queryLoading) {
    return <Spin />;
  }

  if (queryError) {
    return <RequestFailedError />;
  }

  if (item == null) {
    return <Empty />;
  }

  return (
    <Card className="narrow-layout">
      <Descriptions
        layout="horizontal"
        title={getPetDtoDisplayName(item)}
        column={1}
      >
        <Descriptions.Item label={<strong>Identification Number</strong>}>
          {item.identificationNumber ?? undefined}
        </Descriptions.Item>
        <Descriptions.Item label={<strong>Birth Date</strong>}>
          {item.birthDate?.format("LL") ?? undefined}
        </Descriptions.Item>
        <Descriptions.Item label={<strong>Type</strong>}>
          {getPetTypeDtoDisplayName(item.type ?? undefined)}
        </Descriptions.Item>
        <Descriptions.Item label={<strong>Owner</strong>}>
          {getOwnerDtoDisplayName(item.owner ?? undefined)}
        </Descriptions.Item>
        <Descriptions.Item label={<strong>Description</strong>}>
          {getPetDescriptionDtoDisplayName(item.description ?? undefined)}
        </Descriptions.Item>
        <Descriptions.Item label={<strong>Tags</strong>}>
          {item.tags &&
            item.tags
              .map(entry => getTagDtoDisplayName(entry))
              .filter(entry => entry !== "")
              .join(", ")}
        </Descriptions.Item>
        <Descriptions.Item label={<strong>Diseases</strong>}>
          {item.diseases &&
            item.diseases
              .map(entry => getPetDiseaseDtoDisplayName(entry))
              .filter(entry => entry !== "")
              .join(", ")}
        </Descriptions.Item>
      </Descriptions>
      <Button htmlType="button" onClick={() => navigate("..")}>
        <FormattedMessage id="common.close" />
      </Button>
    </Card>
  );
}
