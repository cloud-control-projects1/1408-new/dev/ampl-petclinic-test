### Serving Frontend

In most cases, you'll want to serve frontend as static resources 
via a web severer (e.g. nginx/Apache).
However, it's possible to serve the frontend straight from the Spring Boot app.
Use `-DincludeFrontend=true` property to include the frontend to the backend build pipeline.
You can customize the context path of the frontend using the `BASE_URL` env variable. 

* To serve the frontend from the root:

    ```
    ./mvnw clean spring-boot:run -DincludeFrontend=true
    ```

* To serve the frontend from the other path:
    ```
    BASE_URL=/front/; mvnw clean spring-boot:run -DincludeFrontend=true
    ```
  In this case, it is required to set a value for the `app.frontend.baseUrl` property in your `application.properties`:
  ```groovy
  application.frontend.baseUrl=front
  ```  

If an environment variable `BASE_URL` is not set, the default value `/` is used.