package io.jmix2mvp.petclinic.entity;

public enum ProtectionStatus {
    NO_DANGER,
    NEEDS_PROTECTION,
    RED_BOOK
}
