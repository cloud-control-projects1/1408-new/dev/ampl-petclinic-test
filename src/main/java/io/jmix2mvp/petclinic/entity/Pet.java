package io.jmix2mvp.petclinic.entity;

import io.jmix2mvp.petclinic.entity.constraints.PetTestConstraint1;
import io.jmix2mvp.petclinic.entity.constraints.PetTestConstraint2;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "pet")
@PetTestConstraint1
@PetTestConstraint2
public class Pet extends BaseEntity {
    @Embedded
    private NewEntity newEntity;

    @Column(name = "identification_number", nullable = false)
    @Pattern(regexp = "^[a-zA-Z0-9_]*$")
    @Size(max = 5)
    @NotBlank(message = "{Pet.identificationNumber.notBlank}")
    private String identificationNumber;

    @Column(name = "birth_date")
    @Past
    private LocalDate birthDate;

    @Min(0)
    private Integer weightInGrams;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id")
    private PetType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id")
    private Owner owner;

    @ManyToMany
    @JoinTable(name = "pet_tags",
            joinColumns = @JoinColumn(name = "pet_id"),
            inverseJoinColumns = @JoinColumn(name = "tags_id"))
    private List<Tag> tags = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pet_description_id")
    private PetDescription description;

    @ManyToMany
    @JoinTable(
            name = "pet_diseases",
            joinColumns = @JoinColumn(name = "pet_id"),
            inverseJoinColumns = @JoinColumn(name = "pet_diseases_id")
    )
    private List<PetDisease> diseases = new ArrayList<>();

    public NewEntity getNewEntity() {
        return newEntity;
    }

    public void setNewEntity(NewEntity newEntity) {
        this.newEntity = newEntity;
    }

    public List<PetDisease> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<PetDisease> diseases) {
        this.diseases = diseases;
    }

    public PetDescription getDescription() {
        return description;
    }

    public void setDescription(PetDescription description) {
        this.description = description;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public PetType getType() {
        return type;
    }

    public void setType(PetType type) {
        this.type = type;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getWeightInGrams() {
        return weightInGrams;
    }

    public void setWeightInGrams(Integer visitCount) {
        this.weightInGrams = visitCount;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }
}