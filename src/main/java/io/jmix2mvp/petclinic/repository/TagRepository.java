package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {
}