variable "name" {
  type        = string
  default     = "amplicode-mvp-petclinic"
  description = "Application name"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "use_private_subnets" {
  type    = bool
  default = false
}

variable "azs_count" {
  type        = number
default     = 2

  description = "Number of Availability Zones to be used"
}

variable "instance_type" {
  type    = string
  default = "t2.medium"
}

variable "region" {
  type    = string
  default = "us-east-2"
}

variable "image" {
  type    = string
}

variable "ports" {
  type    = list(number)
  default = [ 8080 ]
}

locals {
  env = [
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "AMPLICODE-MVP-PETCLINIC_DB_NAME",
        value = module.amplicode-mvp-petclinic_db.name
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "AMPLICODE-MVP-PETCLINIC_DB_HOST",
        value = module.amplicode-mvp-petclinic_db.host
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "AMPLICODE-MVP-PETCLINIC_DB_PORT",
        value = module.amplicode-mvp-petclinic_db.port
      },
      {
        namespace : "aws:elasticbeanstalk:application:environment",
        name : "AMPLICODE-MVP-PETCLINIC_DB_USER",
        value : module.amplicode-mvp-petclinic_db.username
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "AMPLICODE-MVP-PETCLINIC_DB_PASSWORD",
        value = module.amplicode-mvp-petclinic_db.password
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_DB_NAME",
        value = module.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db.name
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_DB_HOST",
        value = module.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db.host
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_DB_PORT",
        value = module.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db.port
      },
      {
        namespace : "aws:elasticbeanstalk:application:environment",
        name : "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_DB_USER",
        value : module.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db.username
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_DB_PASSWORD",
        value = module.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db.password
      },
      {
        namespace = "aws:elbv2:listener:443",
        name = "ListenerEnabled",
        value = "true"
      },
      {
        namespace = "aws:elbv2:listener:443",
        name = "Protocol",
        value = "HTTPS"
      },
      {
        namespace = "aws:elbv2:listener:443",
        name = "SSLCertificateArns",
        value = module.ssl.certificate_arn
      },
      {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "StreamLogs"
        value = var.enable_logging
      },
      {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "DeleteOnTerminate"
        value = var.delete_logs_on_terminate
      },
      {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "RetentionInDays"
        value = var.logs_retention
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "SPRING_PROFILES_ACTIVE",
        value = "aws"
      }
  ]
}

variable "enable_logging" {
  type    = bool
  default = true
}

variable "delete_logs_on_terminate" {
  type    = bool
  default = true
}

variable "logs_retention" {
  type    = number
  default = 30
}

variable "amplicode-mvp-petclinic_db_name" {
  type    = string
  default = "amplicode-mvp-petclinic"
}

variable "amplicode-mvp-petclinic_db_engine" {
  type    = string
  default = "postgres"
}

variable "amplicode-mvp-petclinic_db_engine_version" {
  type    = string
  default = "15.3"
}

variable "amplicode-mvp-petclinic_db_instance_class" {
  type    = string
  default = "db.t3.small"
}

variable "amplicode-mvp-petclinic_db_storage" {
  type    = number
  default = 10
}

variable "amplicode-mvp-petclinic_db_user" {
  type    = string
  default = "root"
}

variable "amplicode-mvp-petclinic_db_password" {
  type  = string
default = null

}

variable "amplicode-mvp-petclinic_db_random_password" {
  type    = bool
  default = true
}

variable "amplicode-mvp-petclinic_db_multi_az" {
  type    = bool
  default = false
}

variable "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_name" {
  type    = string
  default = "${AMPLICODE-MVP-PETCLINIC_DB_NAME}"
}

variable "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_engine" {
  type    = string
  default = "postgres"
}

variable "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_engine_version" {
  type    = string
  default = "15.3"
}

variable "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_instance_class" {
  type    = string
  default = "db.t3.small"
}

variable "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_storage" {
  type    = number
  default = 10
}

variable "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_user" {
  type    = string
  default = "${AMPLICODE-MVP-PETCLINIC_DB_USER}"
}

variable "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_password" {
  type  = string
default = null

}

variable "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_random_password" {
  type    = bool
  default = true
}

variable "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_multi_az" {
  type    = bool
  default = false
}

