module "amplicode-mvp-petclinic_db" {
  source = "./db"

  name                                  = var.amplicode-mvp-petclinic_db_name
  engine                                = var.amplicode-mvp-petclinic_db_engine
  engine_version                        = var.amplicode-mvp-petclinic_db_engine_version
  instance_class                        = var.amplicode-mvp-petclinic_db_instance_class
  storage                               = var.amplicode-mvp-petclinic_db_storage
  user                                  = var.amplicode-mvp-petclinic_db_user
  password                              = var.amplicode-mvp-petclinic_db_password
  random_password                       = var.amplicode-mvp-petclinic_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.amplicode-mvp-petclinic_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
module "${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db" {
  source = "./db"

  name                                  = var.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_name
  engine                                = var.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_engine
  engine_version                        = var.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_engine_version
  instance_class                        = var.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_instance_class
  storage                               = var.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_storage
  user                                  = var.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_user
  password                              = var.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_password
  random_password                       = var.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.${AMPLICODE-MVP-PETCLINIC_DB_NAME}_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
