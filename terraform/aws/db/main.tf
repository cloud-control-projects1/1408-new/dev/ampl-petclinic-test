resource "aws_security_group" "db_sg" {
  name_prefix = "${var.name}-sg-"
  vpc_id      = var.vpc_id
}

resource "random_password" "additional_db_passwords" {
  count   = var.random_password ? 1 : 0
  length  = 16
  special = false
}

resource "aws_db_instance" "db" {
  db_name                               = var.name
  instance_class                        = var.instance_class
  engine                                = var.engine
  engine_version                        = var.engine_version
  allocated_storage                     = var.storage
  username                              = var.user
  password                              = var.random_password ? random_password.additional_db_passwords[0].result : var.password
  skip_final_snapshot                   = true
  performance_insights_enabled          = var.performance_insights_enabled
  performance_insights_retention_period = var.performance_insights_enabled ? var.performance_insights_retention_period : null
  monitoring_interval                   = var.monitoring_interval
  monitoring_role_arn                   = var.enhanced_monitoring_arn
  enabled_cloudwatch_logs_exports       = var.enabled_cloudwatch_logs_exports

  multi_az = var.multi_az

  vpc_security_group_ids = [aws_security_group.db_sg.id]
  db_subnet_group_name   = var.subnet_group_name
}

resource "aws_security_group_rule" "main_db" {
  type              = "ingress"
  security_group_id = aws_security_group.db_sg.id

  from_port                = 0
  to_port                  = aws_db_instance.db.port
  protocol                 = "tcp"
  source_security_group_id = var.source_security_group_id
}
